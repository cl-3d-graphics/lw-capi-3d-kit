;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "USER")

(defpackage "CAPI-3D-KIT"
  (:use "CAPI" "GP" "SCENE" "OPENGL")
  (:add-use-defaults)
  (:export

   ;;; scene-view
   SCENE-VIEW
   MAKE-SCENE-VIEW

   ;;; RESOURCES
   *EXAMPLE-TEXTURES-DIR*))


;;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "CAPI-3D-KIT")

(defun make-vbo ()
  (let ((buffer-ids (opengl:make-gl-unsigned-int-vector 1)))
    (gl-gen-buffers 1 buffer-ids)
    (gl-vector-aref buffer-ids 0)))

(defun bind-buffer-data (target source draw-type)
  (let ((count (geometry-source-count source))
        (components (geometry-source-n-per-vector source))
        (data-type (geometry-source-data-type source)))
  (gl-buffer-data target
                  (* count components (bytes-per-component data-type))
                  (make-gl-data (geometry-source-data source)
                                count
                                components
                                data-type)
                  (gl-draw-type draw-type))))

(defun bind-attrib-source (source draw-type)
  (let ((vbo (make-vbo)))
    (gl-bind-buffer *gl-array-buffer* vbo)
    (setf (geometry-source-vbo source) vbo)
    (bind-buffer-data *gl-array-buffer* source draw-type)))

(defun bind-index-source (source draw-type)
  (let ((vbo (make-vbo)))
    (gl-bind-buffer *gl-element-array-buffer* vbo)
    (setf (geometry-source-vbo source) vbo)
    (bind-buffer-data *gl-element-array-buffer* source draw-type)))

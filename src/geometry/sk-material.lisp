;;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "CAPI-3D-KIT")

(defconstant *phong* "PHONG")
(defconstant *blinn-phong* "BLINN-PHONG") ;;; DEFUALT LIGHT MODEL

(defconstant *default-light* 0)
(defconstant *custom-lights* 1)
(defconstant *default-ambient* 2)


;;; -----------------
;;; Class definitions
;;; -----------------

(defclass material ()
  ((curr-active-tex-slot :initform 0
                         :initarg :curr-active-tex-slot
                         :accessor material-curr-active-tex-slot)
   (ambient :initform nil
            :initarg :ambient
            :accessor material-ambient)
   (diffuse :initform nil
            :initarg :diffuse
            :accessor material-diffuse)
   (emissive :initform nil
             :initarg :emissive
             :accessor material-emissive)
   (specular :initform nil
             :initarg :specular
             :accessor material-specular)
   (shininess :initform nil
              :initarg :shininess
              :accessor material-shininess)
   (normal :initform nil
           :initarg :normal
           :accessor material-normal)
   (light-model :initform *phong*
                :initarg :light-model
                :accessor material-light-model)))

;;; -----------------
;;; Class methods
;;; -----------------

(defun get-light-model-id (light-model)
  (cond ((equal light-model *blinn-phong*) 1)
        ((equal light-model *phong*) 2)
        (t nil)))

(defun make-default-material ()
  (let ((material (make-instance 'material)))
    (setf (material-ambient material) (make-dark-gray-color)
          (material-diffuse material) (make-white-color)
          (material-emissive material) (make-black-color)
          (material-specular material) (make-black-color)
          (material-normal material) (make-white-color)
          (material-shininess material) 33.0
          (material-light-model material) *blinn-phong*)
    material))

(defun make-material (emissive ambient diffuse specular shininess)
  (let ((material (make-instance 'material)))
    (setf (material-emissive material) emissive
          (material-ambient material) ambient
          (material-diffuse material) diffuse
          (material-specular material) specular
          (material-normal material) (make-white-color)
          (material-shininess material) shininess
          (material-light-model material) *blinn-phong*)
    material))

(defgeneric init-texture (material-property gp)
  (:documentation "Initialize the texture for a material property"))

(defmethod init-texture ((material-property color-rgba) gp) t)

(defmethod init-texture ((material-property texture) gp)
  (load-texture gp material-property)
  (let ((texture-id (make-gl-texture)))
    (setf (texture-id material-property) texture-id)
    (gl-active-texture (get-gl-active-texture-slot
                        (texture-active-slot material-property)))
    (gl-bind-texture *gl-texture-2d* texture-id)
    (gl-tex-image2-d *gl-texture-2d*
                     0
                     *gl-rgba*
                     (texture-width material-property)
                     (texture-height material-property)
                     0
                     *gl-rgba*
                     *gl-unsigned-byte*
                     (texture-gl-pixels material-property))
    (gl-generate-mipmap *gl-texture-2d*)
    (gl-tex-parameteri *gl-texture-2d*
                       *gl-texture-wrap-s*
                       *gl-clamp-to-edge*)
    (gl-tex-parameteri *gl-texture-2d*
                       *gl-texture-wrap-t*
                       *gl-clamp-to-edge*)
    (gl-tex-parameteri *gl-texture-2d*
                       *gl-texture-mag-filter*
                       *gl-linear*)
    (gl-tex-parameteri *gl-texture-2d*
                       *gl-texture-min-filter*
                       *gl-linear-mipmap-linear*)))

(defun init-material-textures (material canvas)
  (init-texture (material-emissive material) canvas)
  (init-texture (material-ambient material ) canvas)
  (init-texture (material-diffuse material ) canvas)
  (init-texture (material-specular material) canvas))

(defun make-gl-texture ()
  (let ((texture-ids (make-gl-unsigned-int-vector 1)))
    (gl-gen-textures 1 texture-ids)
    (gl-vector-aref texture-ids 0)))

(defun get-gl-active-texture-slot (slot-id)
  (symbol-value (find-symbol (concatenate 'string
                                          "*GL-TEXTURE"
                                          (write-to-string slot-id)
                                          "*")
                             (find-package "OPENGL"))))

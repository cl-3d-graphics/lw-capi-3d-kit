;;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "CAPI-3D-KIT")

(defconstant *rgba-comp-size* 4)

(defun make-gl-pixels (size pixels-vec)
  (let* ((gl-pixels (make-gl-vector :unsigned-8 size)))
    (do ((i 0 (+ i 4)))
        ((= i size) gl-pixels)
      ;;; image-access-pixels-to-bgra returns BGRA
      ;;; we need RGBA, thus R and B are read accordingly
      ;;;           RGBA                               BGRA
      (setf (gl-vector-aref gl-pixels i)        (aref pixels-vec (+ i 2))
            (gl-vector-aref gl-pixels (+ i  1)) (aref pixels-vec (+ i 1))
            (gl-vector-aref gl-pixels (+ i  2)) (aref pixels-vec i)
            (gl-vector-aref gl-pixels (+ i  3)) (aref pixels-vec (+ i 3))))))

(defun load-texture (gp texture)
  (let* ((image (load-image gp (texture-path texture)))
         (image-access (make-image-access gp image))
         (height (image-height image))
         (width (image-width image))
         (size (* *rgba-comp-size* height width))
         (pixels-vec (make-array size :element-type '(unsigned-byte 8))))
    ;;; This loads the pixels in image into pixels-vec
    (image-access-transfer-from-image image-access)
    (image-access-pixels-to-bgra image-access pixels-vec)
    (setf (texture-height texture) height
          (texture-width texture) width
          (texture-size texture) size
          (texture-gl-pixels texture) (make-gl-pixels size pixels-vec))))



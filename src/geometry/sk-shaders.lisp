;;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "CAPI-3D-KIT")

;;; These shaders are used when no light is assigned
(defparameter *no-light-vertex-shader*   "no-light-vs.glsl")
(defparameter *no-light-fragment-shader* "no-light-fs.glsl")

;;; These shaders are used when light is assigned
(defparameter *light-vertex-shader*   "light-vs.glsl")
(defparameter *light-fragment-shader* "light-fs.glsl")

(defconstant *max-lights* 8)

;;; -----------------
;;; Class methods
;;; -----------------

(defun assign-shader-program (geometry light-type)
  (setf (geometry-shader-program geometry)
        (if light-type
            (make-shader-program *scene-kit-shaders-dir*
                                 *light-vertex-shader*
                                 *light-fragment-shader*
                                 #'light-shader-locs)
            (make-shader-program *scene-kit-shaders-dir*
                                 *no-light-vertex-shader*
                                 *no-light-fragment-shader*
                                 #'no-light-shader-locs))))

(defun compile-shader (shader shader-string)
  (gl-shader-source shader
                    1
                    (convert-to-dynamic-foreign-array
                     (list shader-string))
                    nil)
  (gl-compile-shader shader))

(defun compile-vertex-shader (shader-program)
  (let ((vs (gl-create-shader *gl-vertex-shader*))
        (vs-string (shader-program-vertex-shader shader-program)))
    (setf (shader-program-vs shader-program) vs)
    (compile-shader vs vs-string)))

(defun compile-fragment-shader (shader-program)
  (let ((fs (gl-create-shader *gl-fragment-shader*))
        (fs-string (shader-program-fragment-shader shader-program)))
    (setf (shader-program-fs shader-program) fs)
    (compile-shader fs fs-string)))

(defun compile-shaders (shader-program)
  (compile-vertex-shader shader-program)
  (compile-fragment-shader shader-program))

(defun compile-shader-program (geometry model-mat view-mat proj-mat
                               light-model ambient-light lights)
  (let ((sp (gl-create-program))
        (shader-program (geometry-shader-program geometry)))
    (setf (shader-program-sp shader-program) sp)
    (compile-shaders shader-program)
    (gl-attach-shader sp (shader-program-vs shader-program))
    (gl-attach-shader sp (shader-program-fs shader-program))
    (gl-link-program sp)
    (let ((locs-fn (shader-program-locs-fn shader-program)))
      (if locs-fn
          (funcall locs-fn geometry model-mat view-mat proj-mat
                   light-model ambient-light lights)))))

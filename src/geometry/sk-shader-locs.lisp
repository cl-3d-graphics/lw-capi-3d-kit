;;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "CAPI-3D-KIT")

(defgeneric assign-material (color-loc use-tex-loc tex-loc material-property)
  (:documentation "Assign material color or texture to loc"))

(defmethod assign-material (color-loc
                            use-tex-loc
                            tex-loc
                            (material-property color-rgba))
  (gl-uniform1-i use-tex-loc 0)
  (let ((r (color-rgba-r material-property))
        (g (color-rgba-g material-property))
        (b (color-rgba-b material-property)))
    (gl-uniform3-f color-loc r g b)))

(defmethod assign-material (color-loc
                            use-tex-loc
                            tex-loc
                            (material-property texture))
  (gl-uniform1-i use-tex-loc 1)
  (gl-uniform1-i tex-loc (texture-active-slot material-property)))

;;; ------------ ---------------------------------------------------------------
;;; THIS IS USED WHEN NO LIGHT IS USED
;;; ------------ ---------------------------------------------------------------
(defun no-light-shader-locs (geometry model-mat view-mat proj-mat
                             light-model ambient-light lights)
  (declare (ignore light-model ambient-light lights))
  (let* ((shader-program (geometry-shader-program geometry))
         (sp (shader-program-sp shader-program))
         (model-loc (gl-get-uniform-location sp "model"))
         (view-loc (gl-get-uniform-location sp "view"))
         (proj-loc (gl-get-uniform-location sp "proj"))
         (use-tex-loc (gl-get-uniform-location sp "use_color_texture"))
         (use-color-loc (gl-get-uniform-location sp "use_color"))
         (tex-loc (gl-get-uniform-location sp "basic_texture"))
         (color-loc (gl-get-uniform-location sp "input_color")))
    (gl-use-program sp)
    (gl-uniform-matrix4-fv model-loc 1 *gl-false* model-mat)
    (gl-uniform-matrix4-fv view-loc 1 *gl-false* view-mat)
    (gl-uniform-matrix4-fv proj-loc 1 *gl-false* proj-mat)
    (if (geometry-color-source geometry)
        (gl-uniform1-i use-color-loc 1)
        (let* ((mat (geometry-material geometry))
               (diffuse (material-diffuse mat)))
            (assign-material color-loc use-tex-loc tex-loc diffuse)))))

;;; ------------ ---------------------------------------------------------------
;;; THIS IS USED WHEN LIGHT IS USED
;;; ------------ ---------------------------------------------------------------
(defun light-shader-locs (geometry model-mat view-mat proj-mat
                          light-model ambient-light lights)
  (let* ((shader-program (geometry-shader-program geometry))
         (sp (shader-program-sp shader-program))
         (model-loc (gl-get-uniform-location sp "model_mat"))
         (view-loc (gl-get-uniform-location sp "view_mat"))
         (proj-loc (gl-get-uniform-location sp "projection_mat"))
         (light-model-loc (gl-get-uniform-location sp "light_model"))
         (use-color-loc (gl-get-uniform-location sp "use_color"))
         (use-ka-tex-loc (gl-get-uniform-location sp "use_ka_texture"))
         (ka-tex-loc (gl-get-uniform-location sp "ka_texture"))
         (ka-loc (gl-get-uniform-location sp "Ka"))
         (use-kd-tex-loc (gl-get-uniform-location sp "use_kd_texture"))
         (kd-tex-loc (gl-get-uniform-location sp "kd_texture"))
         (kd-loc (gl-get-uniform-location sp "Kd"))
         (use-ke-tex-loc (gl-get-uniform-location sp "use_ke_texture"))
         (ke-tex-loc (gl-get-uniform-location sp "ke_texture"))
         (ke-loc (gl-get-uniform-location sp "Ke"))
         (use-ks-tex-loc (gl-get-uniform-location sp "use_ks_texture"))
         (ks-tex-loc (gl-get-uniform-location sp "ks_texture"))
         (ks-loc (gl-get-uniform-location sp "Ks"))
         (use-nor-tex-loc (gl-get-uniform-location sp "use_normal_texture"))
         (nor-tex-loc (gl-get-uniform-location sp "normal_texture"))
         (nor-loc (gl-get-uniform-location sp "Ncolor"))
         (shininess-loc (gl-get-uniform-location sp "specular_exponent"))
         (amb-light-loc (gl-get-uniform-location sp "ambient_light")))
    (gl-use-program sp)
    (gl-uniform-matrix4-fv model-loc 1 *gl-false* model-mat)
    (gl-uniform-matrix4-fv view-loc 1 *gl-false* view-mat)
    (gl-uniform-matrix4-fv proj-loc 1 *gl-false* proj-mat)
    (gl-uniform1-i light-model-loc light-model)
    (if (geometry-color-source geometry)
        (gl-uniform1-i use-color-loc 1)
        (gl-uniform1-i use-color-loc 0))
    (let* ((mat (geometry-material geometry))
           (ambient (material-ambient mat))
           (diffuse (material-diffuse mat))
           (specular (material-specular mat))
           (shininess (material-shininess mat))
           (emissive (material-emissive mat))
           (normal (material-normal mat)))
        (assign-material ka-loc use-ka-tex-loc ka-tex-loc ambient)
        (assign-material kd-loc use-kd-tex-loc kd-tex-loc diffuse)
        (assign-material ke-loc use-ke-tex-loc ke-tex-loc emissive)
        (assign-material ks-loc use-ks-tex-loc ks-tex-loc specular)
        (assign-material nor-loc use-nor-tex-loc nor-tex-loc normal)
        (gl-uniform1-f shininess-loc shininess))
    (if ambient-light
        (let* ((color (light-color ambient-light))
               (r (color-rgba-r color))
               (g (color-rgba-g color))
               (b (color-rgba-b color)))
            (gl-uniform3-f amb-light-loc r g b))
        ;;; default ambient light when none provided is gray
        (gl-uniform3-f amb-light-loc 0.2 0.2 0.2))
    (if lights (assign-light-locs sp lights))))

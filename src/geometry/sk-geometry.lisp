;;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "CAPI-3D-KIT")

(defun init-log-geometry (geometry canvas model view proj
                          light-model light-type ambient-light lights)
  (with-slots ((viewer interface)) canvas
    (with-slots ((show-console show-console) logger) viewer
      (if show-console
          (let* ((shader-program (geometry-shader-program geometry))
                 (vs (shader-program-vs shader-program))
                 (fs (shader-program-fs shader-program))
                 (sp (shader-program-sp shader-program)))
            (gl-log logger "---Initializing geometry ~A~%"
                    (geometry-name geometry))
            (gl-log logger "---Vertex Shader ~% ~A ~%"
                    (shader-program-vertex-shader shader-program))
            (gl-log-shader-info logger vs)
            (gl-log logger "---Fragment Shader ~% ~A ~%"
                    (shader-program-fragment-shader shader-program))
            (gl-log-shader-info logger fs)
            (gl-log logger "---Shader Program ~%")
            (gl-log-program-info logger sp)
            (gl-log-all-program-info logger sp)
            (gl-log logger "---Camera matrices ~%")
            (gl-log-matrix logger "Model:" model)
            (gl-log-matrix logger "View: " view)
            (gl-log-matrix logger "Proj: " proj)
            (gl-log logger "Light Model: ~A~%" light-model)
            (gl-log logger
                    "Light Type (No=NIL/Default=0/Custom=1/Ambient=2) ~A ~%"
                    light-type)
            (if ambient-light
              (gl-log logger "Ambient Light ~A ~A ~%"
                      (light-type ambient-light)
                      ambient-light))
            (gl-log logger "Lights ~A~%" (length lights))
            (dolist (light lights)
              (gl-log logger "Light: ~A ~A ~%"
                      (light-type light)
                      light)))))))

(defun init-geometry (geometry model-mat view-mat proj-mat canvas
                      &optional light-type ambient-light lights)
  (init-vao geometry)
  (init-vbos geometry)
  (init-material-textures (geometry-material geometry) canvas)
  (enable-vao geometry)
  (let* ((material (geometry-material geometry))
         (light-model (material-light-model material)))
    (assign-shader-program geometry light-type)
    (compile-shader-program geometry model-mat view-mat proj-mat
                            (get-light-model-id light-model)
                            ambient-light lights)
    (init-log-geometry geometry canvas model-mat view-mat proj-mat
                       light-model light-type ambient-light lights)))

(defun draw-geometry (geometry model-mat view-mat proj-mat
                      &optional ambient-light lights)
  (let ((index-source (geometry-index-source geometry))
        (shader-program (geometry-shader-program geometry)))
    (gl-use-program (shader-program-sp shader-program))
    (let ((locs-fn (shader-program-locs-fn shader-program))
          (light-model (material-light-model (geometry-material geometry))))
      (if locs-fn
          (funcall locs-fn geometry model-mat view-mat proj-mat
                   (get-light-model-id light-model) ambient-light lights)))
    (gl-bind-vertex-array (geometry-vao geometry))
    (gl-bind-buffer *gl-element-array-buffer*
                    (geometry-source-vbo index-source))
    (when (geometry-cull-face geometry)
        (gl-enable *gl-cull-face*)
        (if (geometry-cull-back-face geometry)
            (gl-cull-face *gl-back*)
            (gl-cull-face *gl-front*))
        (if (geometry-clockwise geometry)
            (gl-front-face *gl-cw*)
            (gl-front-face *gl-ccw*)))
    (gl-draw-elements *gl-triangles*
                        (geometry-source-count index-source)
                        *gl-unsigned-int*
                        fli:*null-pointer*)
    (when (geometry-cull-face geometry)
      (gl-disable *gl-cull-face*))))

(defun init-vao (geometry)
  (let ((buffer-ids (make-gl-unsigned-int-vector 1)))
    (gl-gen-vertex-arrays 1 buffer-ids)
    (setf (geometry-vao geometry)
          (gl-vector-aref buffer-ids 0))))

(defun init-vbos (geometry)
  (let ((vertex-source (geometry-vertex-source geometry))
        (color-source (geometry-color-source geometry))
        (diffuse-source (geometry-diffuse-source geometry))
        (specular-source (geometry-specular-source geometry))
        (ambient-source (geometry-ambient-source geometry))
        (emissive-source (geometry-emissive-source geometry))
        (normal-source (geometry-normal-source geometry))
        (index-source (geometry-index-source geometry))
        (draw-type (geometry-draw-type geometry)))
    (bind-attrib-source vertex-source draw-type)
    (if color-source (bind-attrib-source color-source draw-type))
    (if diffuse-source (bind-attrib-source diffuse-source draw-type))
    (if specular-source (bind-attrib-source specular-source draw-type))
    (if ambient-source (bind-attrib-source ambient-source draw-type))
    (if emissive-source (bind-attrib-source emissive-source draw-type))
    (if normal-source (bind-attrib-source normal-source draw-type))
    (bind-index-source index-source draw-type)))

(defun enable-vao-for-source (source)
  (let ((index (geometry-source-index source)))
    (gl-enable-vertex-attrib-array index)
    (gl-bind-buffer *gl-array-buffer*
                    (geometry-source-vbo source))
    (gl-vertex-attrib-pointer index
                              (geometry-source-n-per-vector source)
                              (data-type->gl (geometry-source-data-type source))
                              *gl-false*
                              0
                              nil)))

(defun enable-vao (geometry)
  (let ((vertex-source (geometry-vertex-source geometry))
        (color-source (geometry-color-source geometry))
        (diffuse-source (geometry-diffuse-source geometry))
        (specular-source (geometry-specular-source geometry))
        (ambient-source (geometry-ambient-source geometry))
        (emissive-source (geometry-emissive-source geometry))
        (normal-source (geometry-normal-source geometry)))
    (gl-bind-vertex-array (geometry-vao geometry))
    (enable-vao-for-source vertex-source)
    (if color-source (enable-vao-for-source color-source))
    (if diffuse-source (enable-vao-for-source diffuse-source))
    (if specular-source (enable-vao-for-source specular-source))
    (if ambient-source (enable-vao-for-source ambient-source))
    (if emissive-source (enable-vao-for-source emissive-source))
    (if normal-source (enable-vao-for-source normal-source))))

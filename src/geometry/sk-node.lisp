;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "CAPI-3D-KIT")

(defconstant *tx-idx* 12)
(defconstant *ty-idx* 13)
(defconstant *tz-idx* 14)

(defconstant *sx-idx* 0)
(defconstant *sy-idx* 5)
(defconstant *sz-idx* 10)

(defmethod node-position-mat4 ((node scene-node))
  (let ((pos-mat4 (slot-value node 'position-mat4)))
    (if (null pos-mat4) (make-identity-matrix) pos-mat4)))

(defmethod node-scale-mat4 ((node scene-node))
  (let ((scale-mat4 (slot-value node 'scale-mat4)))
    (if (null scale-mat4) (make-identity-matrix) scale-mat4)))

(defmethod node-rotation-mat4 ((node scene-node))
  (let ((rotation-mat4 (slot-value node 'rotation-mat4)))
    (if (null rotation-mat4) (make-identity-matrix) rotation-mat4)))

(defmethod node-local-mat4 ((node scene-node))
  (let ((local-mat4 (slot-value node 'local-mat4)))
    (if (null local-mat4) (make-identity-matrix) local-mat4)))

(defmethod node-world-mat4 ((node scene-node))
  (let ((world-mat4 (slot-value node 'world-mat4)))
    (if (null world-mat4) (make-identity-matrix) world-mat4)))

(defmethod (setf node-position) (new-position (node scene-node))
  (setf (slot-value node 'position) new-position)
  (let ((pos-mat4 (node-position-mat4 node)))
    (setf (gl-vector-aref pos-mat4 *tx-idx*) (float (vec3-x new-position))
          (gl-vector-aref pos-mat4 *ty-idx*) (float (vec3-y new-position))
          (gl-vector-aref pos-mat4 *tz-idx*) (float (vec3-z new-position))
          (node-position-mat4 node) pos-mat4
          ;;; This local mat4 will evolve into T * R * S
          (node-local-mat4 node) (make-model-matrix
                                  pos-mat4
                                  (node-rotation-mat4 node)
                                  (node-scale-mat4 node)))
    (if (node-camera node)
        (compute-view-matrix (node-camera node)
                             new-position
                             (node-rotation-mat4 node)))))

(defmethod (setf node-rotation) (new-rotation (node scene-node))
  (setf (slot-value node 'rotation) new-rotation)
  (let ((versor (rotation-to-quat new-rotation)))
    (if (node-orientation node)
        (setf (node-orientation node) (quat* versor (node-orientation node)))
        (setf (node-orientation node) versor))
    (let ((rot-mat4 (quat-to-matrix (node-orientation node))))
      (setf (node-rotation-mat4 node) rot-mat4
             ;;; This local mat4 will evolve into T * R * S
            (node-local-mat4 node) (make-model-matrix
                                    (node-position-mat4 node)
                                    rot-mat4
                                    (node-scale-mat4 node)))
      ;;; recompute the up, right, fwd vectors 
      (setf (node-fwd node)   (mat4*vec4 rot-mat4 (node-fwd node))
            (node-right node) (mat4*vec4 rot-mat4 (node-right node))
            (node-up node)    (mat4*vec4 rot-mat4 (node-up node)))
      (if (node-camera node)
          (compute-view-matrix (node-camera node)
                               (node-position-mat4 node)
                               rot-mat4)))))

(defmethod (setf node-scale) (new-scale (node scene-node))
  (setf (slot-value node 'scale) new-scale)
  (let ((scale-mat4 (node-scale-mat4 node)))
    (setf (gl-vector-aref scale-mat4 *sx-idx*) (float (vec3-x new-scale))
          (gl-vector-aref scale-mat4 *sy-idx*) (float (vec3-y new-scale))
          (gl-vector-aref scale-mat4 *sz-idx*) (float (vec3-z new-scale))
          (node-scale-mat4 node) scale-mat4
          ;;; This local mat4 will evolve into T * R * S
          (node-local-mat4 node) (make-model-matrix
                                  (node-position-mat4 node)
                                  (node-rotation-mat4 node)
                                  scale-mat4))))

(defmethod (setf node-camera) (new-camera (node scene-node))
  (setf (slot-value node 'camera) new-camera)
  (compute-view-matrix (node-camera node)
                       (node-position-mat4 node)
                       (node-rotation-mat4 node)))

(defun compute-world-matrix (node)
  (let ((parent (node-parent node)))
    (if parent
        (let ((local-mat4 (node-local-mat4 node))
              (parent-world-mat4 (node-world-mat4 parent)))
          (setf (node-world-mat4 node)
                (matrix* parent-world-mat4 local-mat4))))))

(defun init-log-node (node canvas)
  (with-slots ((viewer interface)) canvas
    (with-slots ((show-console show-console) logger) viewer
      (if show-console
          (gl-log logger "*** Initializing node : ~A ~%"
                  (node-name node))))))

(defun init-node (node camera canvas &optional light-type ambient-light lights)
  (let ((geometry (node-geometry node)))
    (init-log-node node canvas)
    (when geometry
      (setf (node-world-mat4 node) (compute-world-matrix node))
      (init-geometry geometry
                     (node-world-mat4 node)
                     (camera-view-mat camera)
                     (camera-proj-mat camera)
                     canvas
                     light-type
                     ambient-light
                     lights))
    (dolist (child (node-children node))
      (init-node child camera canvas))))

(defun draw-node (node camera &optional ambient-light lights)
  (let* ((geometry (node-geometry node)))
    (when geometry
      (setf (node-world-mat4 node) (compute-world-matrix node))
      (draw-geometry geometry
                     (node-world-mat4 node)
                     (camera-view-mat camera)
                     (camera-proj-mat camera)
                     ambient-light
                     lights))
    (dolist (child (node-children node))
      (draw-node child camera))))

;;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "CAPI-3D-KIT")

(defun gl-draw-type (draw-type)
  (case draw-type
    (:static-draw  *gl-static-draw*)
    (:dynamic-draw *gl-dynamic-draw*)
    (:stream-draw  *gl-stream-draw*)))

(defun bytes-per-component (data-type)
  (case data-type
    (:float 4)
    (:double 8)
    (:unsigned-short 2)
    (:unsigned-int 4)))

(defun data-type->gl (data-type)
  (case data-type
    (:float *gl-float*)
    (:double *gl-double*)
    (:unsigned-short *gl-unsigned-short*)
    (:unsigned-int *gl-unsigned-int*)))

(defun data-type->gl-vector-data-type (data-type)
  (case data-type
    (:float :float)
    (:double :double)
    (:unsigned-short :unsigned-16)
    (:unsigned-int :unsigned-32)))

(defun make-gl-data (data count components data-type)
  (make-gl-vector (data-type->gl-vector-data-type data-type)
                  (* count components)
                  :contents data))

;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "CAPI-3D-KIT")

(defun make-zero-matrix ()
  (make-gl-vector :float
                  16
                  :contents (make-list 16 :initial-element 0.0)))

(defun make-identity-matrix ()
  (make-gl-vector :float
                  16
                  :contents '(1.0 0.0 0.0 0.0
                              0.0 1.0 0.0 0.0
                              0.0 0.0 1.0 0.0
                              0.0 0.0 0.0 1.0)))

(defconstant *row-indices*
  '((0  (0 1 2 3))
    (1  (4 5 6 7))
    (2  (8 9 10 11))
    (3  (12 13 14 15))))

(defun row-indices (row)
  (cadr (assoc row *row-indices*)))

(defconstant *col-indices*
  '((0  (0 4 8 12))
    (1  (1 5 9 13))
    (2  (2 6 10 14))
    (3  (3 7 11 15))))

(defun col-indices (col)
  (cadr (assoc col *col-indices*)))

(eval-when (:compile-toplevel :execute :load-toplevel)
  (defun make-row-col-indices ()
    (let ((idx 0)
            (indices nil))
        (dotimes (row 4)
        (dotimes (col 4)
            (push (list idx (list row col))
                indices)
            (incf idx)))
        (sort indices #'< :key #'car))))

(defconstant *row-col-indices*
  (make-row-col-indices))

(defun row-col-index (elt-idx)
  (let ((row-col (cadr (assoc elt-idx
                              *row-col-indices*))))
    (values (car row-col)
            (cadr row-col))))

(defun matrix* (mat1 mat2)
  (let ((mat (make-zero-matrix)))
    (dotimes (idx 16)
      (multiple-value-bind (row col)
          (row-col-index idx)
        (let ((mults (mapcar #'(lambda (r c)
                                 (* (gl-vector-aref mat1 r)
                                    (gl-vector-aref mat2 c)))
                             (row-indices row)
                             (col-indices col))))
          (setf (gl-vector-aref mat idx)
                (apply #'+ mults)))))
    mat))

(defun make-model-matrix (tr ro sc)
  (matrix* tr (matrix* ro sc)))

(defun gl-matrix-elt (mat idx)
  (gl-vector-aref mat idx))

(defun gl-log-matrix-row (log-pane mat idx1 idx2 idx3 idx4)
  (gl-log log-pane "[~,2f] [~,2f] [~,2f] [~,2f]~%"
                   (gl-matrix-elt mat idx1)
                   (gl-matrix-elt mat idx2)
                   (gl-matrix-elt mat idx3)
                   (gl-matrix-elt mat idx4)))

(defun gl-log-matrix (log-pane name mat)
  (gl-log log-pane " Matrix [Column Major Order]: ~A ~%" name)
  (gl-log-matrix-row log-pane mat 0 4 8 12)
  (gl-log-matrix-row log-pane mat 1 5 9 13)
  (gl-log-matrix-row log-pane mat 2 6 10 14)
  (gl-log-matrix-row log-pane mat 3 7 11 15))

(defun print-matrix-row (mat idx1 idx2 idx3 idx4)
  (format t "[~,2f] [~,2f] [~,2f] [~,2f]~%"
          (gl-matrix-elt mat idx1)
          (gl-matrix-elt mat idx2)
          (gl-matrix-elt mat idx3)
          (gl-matrix-elt mat idx4)))

(defun print-matrix (name mat)
  (format t " Matrix [Column Major Order]: ~A ~%" name)
  (print-matrix-row mat 0 4 8 12)
  (print-matrix-row mat 1 5 9 13)
  (print-matrix-row mat 2 6 10 14)
  (print-matrix-row mat 3 7 11 15))

(defun make-translation-matrix (pos-vec)
  (let ((mat (make-identity-matrix)))
    (setf (gl-vector-aref mat *tx-idx*) (vec3-x pos-vec)
          (gl-vector-aref mat *ty-idx*) (vec3-y pos-vec)
          (gl-vector-aref mat *tz-idx*) (vec3-z pos-vec))
    mat))

(defun determinant (mat)
  (let ((m0  (gl-vector-aref mat 0))  (m1  (gl-vector-aref mat 1))
        (m2  (gl-vector-aref mat 2))  (m3  (gl-vector-aref mat 3))
        (m4  (gl-vector-aref mat 4))  (m5  (gl-vector-aref mat 5))
        (m6  (gl-vector-aref mat 6))  (m7  (gl-vector-aref mat 7))
        (m8  (gl-vector-aref mat 8))  (m9  (gl-vector-aref mat 9))
        (m10 (gl-vector-aref mat 10)) (m11 (gl-vector-aref mat 11))
        (m12 (gl-vector-aref mat 12)) (m13 (gl-vector-aref mat 13))
        (m14 (gl-vector-aref mat 14)) (m15 (gl-vector-aref mat 15)))
    (let ((d1  (* m12  m9   m6   m3 )) ;; -
		      (d2  (* m8   m13  m6   m3 )) ;; -
          (d3  (* m12  m5   m10  m3 )) ;; +
          (d4  (* m4   m13  m10  m3 )) ;; +
          (d5  (* m8   m5   m14  m3 )) ;; -
          (d6  (* m4   m9   m14  m3 )) ;; -
          (d7  (* m12  m9   m2   m7 )) ;; +
          (d8  (* m8   m13  m2   m7 )) ;; +
          (d9  (* m12  m1   m10  m7 )) ;; -
          (d10 (* m0   m13  m10  m7 )) ;; -
          (d11 (* m8   m1   m14  m7 )) ;; +
          (d12 (* m0   m9   m14  m7 )) ;; +
          (d13 (* m12  m5   m2   m11)) ;; -
          (d14 (* m4   m13  m2   m11)) ;; -
          (d15 (* m12  m1   m6   m11)) ;; +
          (d16 (* m0   m13  m6   m11)) ;; +
          (d17 (* m4   m1   m14  m11)) ;; -
          (d18 (* m0   m5   m14  m11)) ;; -
          (d19 (* m8   m5   m2   m15)) ;; +
          (d20 (* m4   m9   m2   m15)) ;; +
          (d21 (* m8   m1   m6   m15)) ;; -
          (d22 (* m0   m9   m6   m15)) ;; -
          (d23 (* m4   m1   m10  m15)) ;; +
          (d24 (* m0   m5   m10  m15)))
      (- (+ d1 d4 d5 d8 d9 d12 d13 d16 d17 d20 d21 d24)
         (+ d2 d3 d6 d7 d10 d11 d14 d15 d18 d19 d22 d23)))))

(defun inverse (mat)
	(let ((det (determinant mat)))
	  ;;  no inverse if determinant is zero, not likely unless scale is broken
    (if (zerop det) 
        (progn
            (format t "WARNING. matrix has no determinant. can not invert~%")
            mat)
        (let* ((m0  (gl-vector-aref mat 0))  (m1  (gl-vector-aref mat 1))
               (m2  (gl-vector-aref mat 2))  (m3  (gl-vector-aref mat 3))
               (m4  (gl-vector-aref mat 4))  (m5  (gl-vector-aref mat 5))
               (m6  (gl-vector-aref mat 6))  (m7  (gl-vector-aref mat 7))
               (m8  (gl-vector-aref mat 8))  (m9  (gl-vector-aref mat 9))
               (m10 (gl-vector-aref mat 10)) (m11 (gl-vector-aref mat 11))
               (m12 (gl-vector-aref mat 12)) (m13 (gl-vector-aref mat 13))
               (m14 (gl-vector-aref mat 14)) (m15 (gl-vector-aref mat 15))
               (inv (make-zero-matrix))
               (inv-det (/ 1.0 det))
               (a (* inv-det
                   (- (+ (* m9 m14 m7) (* m13 m6 m11) (* m5 m10 m15))
                      (+ (* m13 m10 m7) (* m5 m14 m11) (* m9 m6 m15)))))
               (b (* inv-det
                   (- (+ (* m13 m10 m3) (* m1 m14 m11) (* m9 m2 m15))
                      (+ (* m9 m14 m3) (* m13 m2 m11) (* m1 m10 m15)))))
               (c (* inv-det
                   (- (+ (* m5 m14 m3) (* m13 m2 m7) (* m1 m6 m15))
                      (+ (* m13 m6 m3) (* m1 m14 m7) (* m5 m2 m15)))))
               (d (* inv-det
                   (- (+ (* m9 m6 m3) (* m1 m10 m7) (* m5 m2 m11))
                      (+ (* m5 m10 m3) (* m9 m2 m7) (* m1 m6 m11)))))
               (e (* inv-det
                   (- (+ (* m12 m10 m7) (* m4 m14 m11) (* m8 m6 m15))
                      (+ (* m8 m14 m7) (* m12 m6 m11) (* m4 m10 m15)))))
               (f (* inv-det
                   (- (+ (* m8 m14 m3) (* m12 m2 m11) (* m0 m10 m15))
                      (+ (* m12 m10 m3) (* m0 m14 m11) (* m8 m2 m15)))))
               (g (* inv-det
                   (- (+ (* m12 m6 m3) (* m0 m14 m7) (* m4 m2 m15))
                      (+ (* m4 m14 m3) (* m12 m2 m7) (* m0 m6 m15)))))
               (h (* inv-det
                   (- (+ (* m4 m10 m3) (* m8 m2 m7) (* m0 m6 m11))
                      (+ (* m8 m6 m3) (* m0 m10 m7) (* m4 m2 m11)))))
               (i (* inv-det
                   (- (+ (* m8 m13 m7) (* m12 m5 m11) (* m4 m9 m15))
                      (+ (* m12 m9 m7) (* m4 m13 m11) (* m8 m5 m15)))))
               (j (* inv-det
                   (- (+ (* m12 m9 m3) (* m0 m13 m11) (* m8 m1 m15))
                      (+ (* m8 m13 m3) (* m12 m1 m11) (* m0 m9 m15)))))
               (k (* inv-det
                   (- (+ (* m4 m13 m3) (* m12 m1 m7) (* m0 m5 m15))
                      (+ (* m12 m5 m3) (* m0 m13 m7) (* m4 m1 m15)))))
               (l (* inv-det
                   (- (+ (* m8 m5 m3) (* m0 m9 m7) (* m4 m1 m11))
                      (+ (* m4 m9 m3) (* m8 m1 m7) (* m0 m5 m11)))))
               (m (* inv-det
                   (- (+ (* m12 m9 m6) (* m4 m13 m10) (* m8 m5 m14))
                      (+ (* m8 m13 m6) (* m12 m5 m10) (* m4 m9 m14)))))
               (n (* inv-det
                   (- (+ (* m8 m13 m2) (* m12 m1 m10) (* m0 m9 m14))
                      (+ (* m12 m9 m2) (* m0 m13 m10) (* m8 m1 m14)))))
               (o (* inv-det
                   (- (+ (* m12 m5 m2) (* m0 m13 m6) (* m4 m1 m14))
                      (+ (* m4 m13 m2) (* m12 m1 m6) (* m0 m5 m14)))))
               (p (* inv-det
                   (- (+ (* m4 m9 m2) (* m8 m1 m6) (* m0 m5 m10))
                      (+ (* m8 m5 m2) (* m0 m9 m6) (* m4 m1 m10))))))
            (setf (gl-vector-aref inv 0 ) a
                  (gl-vector-aref inv 1 ) b
                  (gl-vector-aref inv 2 ) c
                  (gl-vector-aref inv 3 ) d
                  (gl-vector-aref inv 4 ) e
                  (gl-vector-aref inv 5 ) f
                  (gl-vector-aref inv 6 ) g
                  (gl-vector-aref inv 7 ) h
                  (gl-vector-aref inv 8 ) i
                  (gl-vector-aref inv 9 ) j
                  (gl-vector-aref inv 10) k
                  (gl-vector-aref inv 11) l 
                  (gl-vector-aref inv 12) m
                  (gl-vector-aref inv 13) n
                  (gl-vector-aref inv 14) o
                  (gl-vector-aref inv 15) p)
            inv))))

(defun mat4*vec4 (mat vec)
  (let* ((m0  (gl-vector-aref mat 0))  (m1  (gl-vector-aref mat 1))
         (m2  (gl-vector-aref mat 2))  (m3  (gl-vector-aref mat 3))
         (m4  (gl-vector-aref mat 4))  (m5  (gl-vector-aref mat 5))
         (m6  (gl-vector-aref mat 6))  (m7  (gl-vector-aref mat 7))
         (m8  (gl-vector-aref mat 8))  (m9  (gl-vector-aref mat 9))
         (m10 (gl-vector-aref mat 10)) (m11 (gl-vector-aref mat 11))
         (m12 (gl-vector-aref mat 12)) (m13 (gl-vector-aref mat 13))
         (m14 (gl-vector-aref mat 14)) (m15 (gl-vector-aref mat 15))
         (v0  (vec4-x vec))
         (v1  (vec4-y vec))
         (v2  (vec4-z vec))
         (v3  (vec4-w vec))
         (x   (+ (* m0 v0) (* m4 v1) (* m8 v2) (* m12 v3)))
         (y   (+ (* m1 v0) (* m5 v1) (* m9 v2) (* m13 v3)))
         (z   (+ (* m2 v0) (* m6 v1) (* m10 v2) (* m14 v3)))
         (w   (+ (* m3 v0) (* m7 v1) (* m11 v2) (* m15 v3))))
    (make-vector4 x y z w)))

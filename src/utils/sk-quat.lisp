;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "CAPI-3D-KIT")

(defun quat-to-matrix (versor)
  (let* ((q0 (versor-q0 versor))
         (q1 (versor-q1 versor))
         (q2 (versor-q2 versor))
         (q3 (versor-q3 versor))
         (w q0) (x q1) (y q2) (z q3)
         (mat (make-zero-matrix)))
    (setf (gl-vector-aref mat 0)  (-  1.0  (* 2.0  y  y) (* 2.0 z  z))
          (gl-vector-aref mat 1)  (+  (* 2.0  x  y) (* 2.0 w z))
          (gl-vector-aref mat 2)  (-  (* 2.0  x  z) (* 2.0 w y))
          (gl-vector-aref mat 3)  0.0
          (gl-vector-aref mat 4)  (- (* 2.0  x  y) (* 2.0 w z))
          (gl-vector-aref mat 5)  (- 1.0 (* 2.0 x  x) (* 2.0 z z))
          (gl-vector-aref mat 6)  (+ (* 2.0  y  z) (* 2.0 w  x))
          (gl-vector-aref mat 7)  0.0
          (gl-vector-aref mat 8)  (+ (* 2.0  x  z) (* 2.0 w  y))
          (gl-vector-aref mat 9)  (- (* 2.0  y  z) (* 2.0  w  x))
          (gl-vector-aref mat 10) (- 1.0 (* 2.0 x  x) (* 2.0 y  y))
          (gl-vector-aref mat 11) 0.0
          (gl-vector-aref mat 12) 0.0
          (gl-vector-aref mat 13) 0.0
          (gl-vector-aref mat 14) 0.0
          (gl-vector-aref mat 15) 1.0)
    mat))


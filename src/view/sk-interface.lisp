;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "CAPI-3D-KIT")

(defun default-scene-update-callback (elapsed-time-for-frame
                                      total-elapsed-time)
  (random 100))

(defun scene-create-canvas (canvas &rest ignore)
  ignore
  (with-slots ((viewer interface)) canvas
    (with-slots (logger scene (show-console show-console)) viewer
      (setf (scene-view-scene viewer)
            (funcall (scene-view-scene-fn viewer)))
      (rendering-on (canvas)
        (if show-console
            (gl-log-init logger))
        (gl-enable *gl-depth-test*)
        (gl-depth-func *gl-less*)
        (init-scene scene canvas)))))

(defun scene-redisplay-canvas (canvas &rest ignore)
  ignore
  (with-slots ((viewer interface)) canvas
    (with-slots (scene background-color) viewer
      (rendering-on (canvas)
        (let ((r (color-rgba-r background-color))
              (g (color-rgba-g background-color))
              (b (color-rgba-b background-color))
              (a (color-rgba-a background-color)))
          (gl-clear-color r g b a))
        (gl-clear *gl-color-buffer-bit*)
        (gl-clear *gl-depth-buffer-bit*)
        (let ((point-of-view (scene-point-of-view scene)))
          (unless (camera-aspect (node-camera point-of-view))
            (multiple-value-bind (width height)
                (simple-pane-visible-size canvas)
              (change-aspect (node-camera point-of-view) width height))))
        (draw-scene scene canvas)
        (swap-buffers canvas)))))

(defun scene-resize-canvas (canvas x y width height)
  (declare (ignore x y))
  (with-slots ((viewer interface)) canvas
    (with-slots (scene) viewer
      (let ((point-of-view (scene-point-of-view scene)))
        (change-aspect (node-camera point-of-view) width height)))))

(defparameter *scene-default-config*
  (list :rgba t
        :depth nil
        :double-buffered t
        :profile :version-3.2-core))

(defun make-scene-canvas (&key (configuration *scene-default-config*)
                               (min-width 640)
                               (min-height 480)
                               (update-callback #'default-scene-update-callback))
  (make-instance 'opengl-pane
                 :configuration configuration
                 :min-width min-width
                 :min-height min-height
                 :create-callback 'scene-create-canvas
                 :resize-callback 'scene-resize-canvas
                 :display-callback 'scene-redisplay-canvas
                 :update-callback update-callback))

(defun change-scene-view-background-color (data interface)
  (declare (ignore data))
  (multiple-value-bind (color-spec done)
      (prompt-for-color "Change background color")
    (if done
        (setf (scene-view-background-color interface)
              (color-spec->color-rgba color-spec)))))

(defun make-color-picker-toolbar-button ()
  (make-instance 'toolbar-button
                 :image 'color-picker
                 :callback 'change-scene-view-background-color))

(defun add-toolbar-buttons ()
  (list (make-color-picker-toolbar-button)))

(define-interface scene-view (opengl-interface)
  ((scene :initform nil
          :initarg  :scene
          :accessor scene-view-scene)
   (scene-fn :initform nil
             :initarg :scene-fn
             :accessor scene-view-scene-fn)
   (background-color :initarg :background-color
                     :initform nil
                     :accessor scene-view-background-color)))

(defun make-scene-view (&key (scene-fn)
                             (background-color (make-black-color))
                             (configuration *scene-default-config*)
                             (min-width 640)
                             (min-height 480)
                             (update-callback 'default-scene-update-callback)
                             (show-statistics nil)
                             (show-console nil))
  (make-instance 'scene-view
                 :scene-fn scene-fn
                 :background-color background-color
                 :add-tools 'add-toolbar-buttons
                 :show-statistics show-statistics
                 :show-console show-console
                 :canvas (make-scene-canvas :configuration configuration
                                            :min-width min-width
                                            :min-height min-height
                                            :update-callback update-callback)))

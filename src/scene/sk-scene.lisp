;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "CAPI-3D-KIT")

(defun init-scene (scene canvas)
  (let ((root-node (scene-root-node scene)))
    (dolist (node (node-children root-node))
      (multiple-value-bind (use-lighting light-type lights)
          (scene-lighting? scene)
        (init-node node
                   (node-camera (scene-point-of-view scene))
                   canvas
                   (if use-lighting light-type)
                   (scene-ambient-light scene)
                   lights)))))

(defun draw-scene (scene canvas)
  (declare (ignore canvas))
  (let ((root-node (scene-root-node scene)))
    (dolist (node (node-children root-node))
      (draw-node node
                 (node-camera (scene-point-of-view scene))
                 (scene-ambient-light scene)
                 (scene-lights scene)))))

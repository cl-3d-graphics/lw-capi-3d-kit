;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "CAPI-3D-KIT")

(defmethod camera-view-mat ((camera camera))
  (let ((view-mat (slot-value camera 'view-mat)))
    (if (null view-mat) (make-identity-matrix) view-mat)))

(defmethod camera-proj-mat ((camera camera))
  (let ((proj-mat (slot-value camera 'proj-mat)))
    (if (null proj-mat) (make-identity-matrix) proj-mat)))

(defmethod (setf camera-near) (new-position (camera camera))
  (setf (slot-value camera 'near) new-position)
  (setf (camera-proj-mat camera) (compute-projection-matrix camera)))

(defmethod (setf camera-far) (new-position (camera camera))
  (setf (slot-value camera 'far) new-position)
  (setf (camera-proj-mat camera) (compute-projection-matrix camera)))

(defmethod (setf camera-fovy) (new-fovy (camera camera))
  (setf (slot-value camera 'fovy) new-fovy)
  (setf (camera-proj-mat camera) (compute-projection-matrix camera)))

(defmethod (setf camera-aspect) (new-aspect (camera camera))
  (setf (slot-value camera 'aspect) new-aspect)
  (setf (camera-proj-mat camera) (compute-projection-matrix camera)))

(defun compute-view-matrix (camera pos-mat4 rot-mat4)
  (setf (camera-view-mat camera) (matrix* (inverse rot-mat4)
                                          (inverse pos-mat4))))

(defun compute-projection-matrix (camera)
  (let ((near   (camera-near   camera))
        (far    (camera-far    camera))
        (fovy   (camera-fovy   camera))
        (aspect (camera-aspect camera)))
    (if aspect
        (let* ((mat (make-zero-matrix))
               (range (* (tan (* fovy 0.5)) near))
               (sx (/ (* 2.0 near) (+ (* range aspect) (* range aspect))))
               (sy (/ near range))
               (sz (/ (- (+ far near)) (- far near)))
               (pz (/ (- (* 2.0 far near)) (- far near))))
          (setf (gl-vector-aref mat 11) -1.0
                (gl-vector-aref mat 0) sx
                (gl-vector-aref mat 5) sy
                (gl-vector-aref mat 10) sz
                (gl-vector-aref mat 14) pz)
          mat)
        (make-identity-matrix))))

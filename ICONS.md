This file contains all the attributions to the icons. This project exclusively
uses the free icons available from `flaticon dot com`.

Every icon png file is placed under the project root.

Files, Attribution
------------------
paint4.png

<div>Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a>
from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is
licensed by <a href="http://creativecommons.org/licenses/by/3.0/"
<div>Icons made by <a href="http://www.flaticon.com/author
;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "CL-USER")

(defsystem "HELLO-CUBE-TRI"
    ()
    :members
    (("DAE-PARSER" :type :system :root-module nil)
     ("SCENE" :type :system :root-module nil)
     ("CAPI-3D-KIT" :type :system :root-module nil)
     "hello-cube"
     )
    :rules
    ((:in-order-to :compile :all (:requires (:load "DAE-PARSER")))
     (:in-order-to :compile :all (:requires (:load "SCENE")))
     (:in-order-to :compile :all (:requires (:load "CAPI-3D-KIT")))))

;; Copyright (c) 2015 Deepak Surti. All rights reserved.

;******************************************************************************
; THIS IS THE MOST BASIC DEMO, HELLO WORLD EQUIVALENT, FOR DAE VIEWER
;******************************************************************************

(in-package "HELLO-CUBE-TRI")

(defun make-hello-cube ()
  (let* ((cube-dae (merge-pathnames (merge-pathnames #p"cube_triangulate.dae"
                                                     *collada-models-dir*)))
         ;;; PARSE-DAE-FILE WILL EVENTUALLY RETURN A SCENE
         (cube (car (gethash "box-lib" (parse-dae-file cube-dae))))
         (mat (make-default-material)))
    (setf (geometry-material cube) mat)
    cube))

(defun make-camera-node ()
  (let ((camera-node (make-scene-node :name "Default Camera Node"
                                      :position (make-vector3 0.0 0.0 55.0)))
        (camera (make-default-camera)))
    (setf (node-camera camera-node) camera)
    camera-node))

(defun make-cube-scene ()
  (let* ((scene (make-scene))
         (cube (make-hello-cube))
         (cube-node (make-scene-node :geometry cube
                                     :position (make-vector3))))
    (setf (node-scale cube-node) (make-vector3 0.1 0.1 0.1))
    (yaw (deg->rad -60.0) cube-node)
    (add-node scene cube-node)
    (setf (scene-default-light scene) t
          (scene-point-of-view scene) (make-camera-node))
    scene))

(defparameter *scene-config*
  (list :rgba t
        :depth nil
        :double-buffered t
        :profile :version-3.2-core
        :msaa t
        :sample-buffers 2
        :samples 16))

(defun start-hello-cube ()
  (let ((scene-view (make-scene-view :scene-fn #'make-cube-scene
                                     :configuration *scene-config*
                                     :show-console t
                                     :show-statistics t)))
    (capi:display scene-view)))

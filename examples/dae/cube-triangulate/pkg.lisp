;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "USER")

(defpackage "HELLO-CUBE-TRI"
  (:use "CAPI" "DAE-PARSER" "SCENE" "CAPI-3D-KIT")
  (:add-use-defaults)
  (:export
   START-HELLO-CUBE))

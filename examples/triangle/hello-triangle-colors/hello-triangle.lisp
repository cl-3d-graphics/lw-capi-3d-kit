;; Copyright (c) 2015 Deepak Surti. All rights reserved.

;******************************************************************************
; OpenGL 4 Example Code.
; Accompanies written series "Anton's OpenGL 4 Tutorials"
; Email: anton at antongerdelan dot net
; First version 27 Jan 2014
; Copyright Dr Anton Gerdelan, Trinity College Dublin, Ireland.
; See individual libraries for separate legal notices
;******************************************************************************
; Ported to LispWorks CAPI OpenGL interface
; Antons Source: 03_vertex_buffer_objects
; "Hello Triangle With Vertex Colors". Just the basics.
; Email: dmsurti at gmail dot com
;******************************************************************************
; THIS IS THE MOST BASIC DEMO, HELLO WORLD EQUIVALENT
;******************************************************************************
(in-package "HELLO-TRIANGLE-COLORS")

(defun make-hello-triangle ()
  (let ((v1 (make-vertex3  0.0  0.7 0.0))
        (c1 (make-rgba 1.0 0.0 0.0))
        (v2 (make-vertex3  0.7 -0.7 0.0))
        (c2 (make-rgba 0.0 1.0 0.0))
        (v3 (make-vertex3 -0.7 -0.7 0.0))
        (c3 (make-rgba 0.0 0.0 1.0)))
    (setf (pos-color v1) c1
          (pos-color v2) c2
          (pos-color v3) c3)
    (make-triangle v1 v2 v3)))

(defun make-triangle-scene ()
  (let* ((scene (make-scene))
         (triangle (make-hello-triangle))
         (tri-node (make-scene-node :geometry triangle)))
    (add-node scene tri-node)
    scene))

(defun start-hello-triangle ()
  (let ((scene-view (make-scene-view :scene-fn #'make-triangle-scene
                                     :show-console t
                                     :show-statistics t)))
    (capi:display scene-view)))

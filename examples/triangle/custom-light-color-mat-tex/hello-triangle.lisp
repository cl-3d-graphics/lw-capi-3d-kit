;; Copyright (c) 2015 Deepak Surti. All rights reserved.

;******************************************************************************
; OpenGL 4 Example Code.
; Accompanies written series "Anton's OpenGL 4 Tutorials"
; Email: anton at antongerdelan dot net
; First version 27 Jan 2014
; Copyright Dr Anton Gerdelan, Trinity College Dublin, Ireland.
; See individual libraries for separate legal notices
;******************************************************************************
; Ported to LispWorks CAPI OpenGL interface
; Antons Source: 08_phong
; "Hello Triangle With Vertex Colors". Just the basics.
; Email: dmsurti at gmail dot com
;******************************************************************************
;THIS DEMOS THE USE OF 1 AMBIENT LIGHT AND 2 OMNI LIGHTS WITH COLOR AND MATERIAL
;******************************************************************************

(in-package "CUSTOM-LIGHT-COLOR-MAT-TEX")

(defun make-hello-triangle ()
  (let* ((v1 (make-vertex3  0.0  0.7 -1.0 (make-vector3 0.0 0.0 1.0)))
         (c1 (make-rgba 1.0 0.0 0.0))
         (v2 (make-vertex3  0.7 -0.7 -1.0 (make-vector3 0.0 0.0 1.0)))
         (c2 (make-rgba 0.0 1.0 0.0))
         (v3 (make-vertex3 -0.7 -0.7 -1.0 (make-vector3 0.0 0.0 1.0)))
         (c3 (make-rgba 0.0 0.0 1.0))
         (diffuse-texture-path (merge-pathnames "diffuse.png"
                                                *example-textures-dir*))
         (diffuse-texture (make-texture diffuse-texture-path))
         (emissive-texture-path (merge-pathnames "emissive.png"
                                                 *example-textures-dir*))
         (emissive-texture (make-texture emissive-texture-path))
         (mat (make-material emissive-texture
                             (make-dark-gray-color)
                             diffuse-texture
                             (make-white-color)
                             100.0)))
    (setf (pos-color v1) c1
          (pos-color v2) c2
          (pos-color v3) c3)
    (setf (pos-diffuse-st v1) (make-tex-st 0.5 1.0 t)
          (pos-diffuse-st v2) (make-tex-st 1.0 0.0 t)
          (pos-diffuse-st v3) (make-tex-st 0.0 0.0 t)
          (pos-emissive-st v1) (make-tex-st 0.5 1.0 t)
          (pos-emissive-st v2) (make-tex-st 1.0 0.0 t)
          (pos-emissive-st v3) (make-tex-st 0.0 0.0 t))
    (let ((tri (make-triangle v1 v2 v3)))
      ;; uncomment this to use Phong lighting
      ;; (setf (material-light-model mat) *phong*)
      (setf (geometry-material tri) mat)
      tri)))

(defparameter *tri-node-name* (unique-node-name))
(defparameter *scene* nil)
(defparameter *speed* 1.0)

(defun scene-update (elapsed-time-for-frame
                     total-elapsed-time)
  (declare (ignore total-elapsed-time))
  (let* ((tri-node (find-node *scene* *tri-node-name*))
         (position (node-position tri-node))
         (x (+ (* elapsed-time-for-frame *speed*)
               (vec3-x position))))
    (setf (node-position tri-node)
          (make-vector3 x 0.0 0.0))
    (if (> (abs x) 1.0)
        (setf *speed*
              (* -1.0 *speed*)))))

(defun make-triangle-scene ()
  (let* ((scene (make-scene))
         (amb-light (make-ambient-light (make-rgba 0.2 0.2 0.2)))
         (amb-light-node (make-node))
         (omni-light1 (make-omni-light (make-white-color)))
         (omni-light-node1 (make-node))
         (dir-light1 (make-directional-light (make-white-color)))
         (dir-light-node1 (make-node))
         (spot-light1 (make-spot-light (make-rgba 1.0 0.5 0.0)
                                       (deg->rad 25.0)))
         (spot-light-node1 (make-node))
         (triangle (make-hello-triangle))
         (tri-node (make-scene-node :name *tri-node-name*
                                    :position (make-vector3 0.5 0.0 0.0)
                                    :geometry triangle)))
    (setf *scene* scene)
    (add-node scene tri-node)

    ;;; ---------------
    ;;; Ambient Light
    ;;; ---------------
    (setf (node-light amb-light-node) amb-light)
    (add-node scene amb-light-node)

    ;;; -----------------------------------------------
    ;;; 1 Point Light
    ;;; -----------------------------------------------
    (setf (node-position omni-light-node1) (make-vector3  1.0 0.0 2.0)
          (node-light omni-light-node1) omni-light1)

    ;;; -------------------
    ;;; Directional Light 1
    ;;; -------------------
    (setf (node-light dir-light-node1) dir-light1)
    (yaw (deg->rad -180.0) dir-light-node1)
    (pitch (deg->rad -60.0) dir-light-node1)
    (roll (deg->rad 30.0) dir-light-node1)

    ;;; ------------
    ;;; Spot Light 1
    ;;; ------------
    (setf (node-position spot-light-node1) (make-vector3 1.0 0.0 1.0)
          (node-light spot-light-node1) spot-light1)

    (add-node scene omni-light-node1)
    (add-node scene dir-light-node1)
    (add-node scene spot-light-node1)
    scene))

(defun start-hello-triangle ()
  (let ((scene-view (make-scene-view :scene-fn #'make-triangle-scene
                                     :show-console t
                                     :show-statistics t
                                     :update-callback 'scene-update)))
    (capi:display scene-view)))

;; Copyright (c) 2015 Deepak Surti. All rights reserved.

;******************************************************************************
; OpenGL 4 Example Code.
; Accompanies written series "Anton's OpenGL 4 Tutorials"
; Email: anton at antongerdelan dot net
; First version 27 Jan 2014
; Copyright Dr Anton Gerdelan, Trinity College Dublin, Ireland.
; See individual libraries for separate legal notices
;******************************************************************************
; Ported to LispWorks CAPI OpenGL interface
; Antons Source: 08_phong
; "Hello Triangle With Vertex Colors". Just the basics.
; Email: dmsurti at gmail dot com
;******************************************************************************
; THIS DEMOS THE USE OF AMBIENT LIGHT WITH COLOR AND MATERIAL
; IF THERE IS AMBIENT LIGHT PROVIDED, SCENE DEFAULT LIGHTING STATE IS REDUNDANT
;******************************************************************************

(in-package "DEFAULT-AMB-LIGHT-COLOR-MAT-TEX")

(defun make-hello-triangle ()
  (let* ((v1 (make-vertex3  0.0  0.7 0.0 (make-vector3 0.0 0.0 1.0)))
         (c1 (make-rgba 1.0 0.0 0.0))
         (v2 (make-vertex3  0.7 -0.7 0.0 (make-vector3 0.0 0.0 1.0)))
         (c2 (make-rgba 0.0 1.0 0.0))
         (v3 (make-vertex3 -0.7 -0.7 0.0 (make-vector3 0.0 0.0 1.0)))
         (c3 (make-rgba 0.0 0.0 1.0))
         (diffuse-texture-path (merge-pathnames "diffuse.png"
                                                *example-textures-dir*))
         (diffuse-texture (make-texture diffuse-texture-path))
         (ambient-texture-path (merge-pathnames "ambient.jpeg"
                                                 *example-textures-dir*))
         (ambient-texture (make-texture ambient-texture-path))
         (mat (make-material (make-black-color)
                             ambient-texture
                             diffuse-texture
                             (make-white-color)
                             100.0)))
    (setf (pos-color v1) c1
          (pos-color v2) c2
          (pos-color v3) c3
          (pos-diffuse-st v1) (make-tex-st 0.5 1.0 t)
          (pos-diffuse-st v2) (make-tex-st 1.0 0.0 t)
          (pos-diffuse-st v3) (make-tex-st 0.0 0.0 t)
          (pos-ambient-st v1) (make-tex-st 0.5 1.0 t)
          (pos-ambient-st v2) (make-tex-st 1.0 0.0 t)
          (pos-ambient-st v3) (make-tex-st 0.0 0.0 t))
    (let ((tri (make-triangle v1 v2 v3)))
      ;; (setf (material-light-model mat) *phong*)
      (setf (geometry-material tri) mat)
      tri)))

(defun make-triangle-scene ()
  (let* ((scene (make-scene))
         (amb-light (make-ambient-light (make-rgba 0.5 0.5 0.5)))
         (amb-light-node (make-node))
         (triangle (make-hello-triangle))
         (tri-node (make-scene-node :geometry triangle)))
    (add-node scene tri-node)
    (setf (node-light amb-light-node) amb-light)
    (add-node scene amb-light-node)
    ;;; This will have no change in redering with only 1 ambient light
    (setf (scene-default-light scene) t)
    scene))

(defun start-hello-triangle ()
  (let ((scene-view (make-scene-view :scene-fn #'make-triangle-scene
                                     :show-console t
                                     :show-statistics t)))
    (capi:display scene-view)))

;; Copyright (c) 2015 Deepak Surti. All rights reserved.

;******************************************************************************
; OpenGL 4 Example Code.
; Accompanies written series "Anton's OpenGL 4 Tutorials"
; Email: anton at antongerdelan dot net
; First version 27 Jan 2014
; Copyright Dr Anton Gerdelan, Trinity College Dublin, Ireland.
; See individual libraries for separate legal notices
;******************************************************************************
; Ported to LispWorks CAPI OpenGL interface
; Antons Source: 04_mats_and_vecs
; "Vectors and Matrices"
; Email: dmsurti at gmail dot com
;******************************************************************************
; THIS DEMOS THE TRANSFORMATION AND CAMERA MATRICES
;******************************************************************************

(in-package "HELLO-TRIANGLE-MAT")

(defun make-hello-triangle ()
  (let ((v1 (make-vertex3  0.0 0.5 0.0))
        (v2 (make-vertex3  0.5 -0.5 0.0))
        (v3 (make-vertex3 -0.5 -0.5 0.0)))
    (make-triangle v1 v2 v3)))

(defparameter *tri-node-name* (unique-node-name))
(defparameter *scene* nil)
(defparameter *speed* 1.0)

(defun scene-update (elapsed-time-for-frame
                     total-elapsed-time)
  (declare (ignore total-elapsed-time))
  (let* ((tri-node (find-node *scene* *tri-node-name*))
         (position (node-position tri-node))
         (x (+ (* elapsed-time-for-frame *speed*)
               (vec3-x position))))
    (setf (node-position tri-node)
          (make-vector3 x 0.0 0.0))
    (if (> (abs x) 1.0)
        (setf *speed*
              (* -1.0 *speed*)))))

(defun make-triangle-scene ()
  (let* ((scene (make-scene))
         (triangle (make-hello-triangle))
         (tri-node (make-scene-node :name *tri-node-name*
                                    :geometry triangle
                                    :position (make-vector3 0.5 0.0 0.0))))
    (setf *scene* scene)
    (add-node scene tri-node)
    scene))

(defun start-hello-triangle ()
  (let ((scene-view (make-scene-view :scene-fn #'make-triangle-scene
                                     :show-console t
                                     :show-statistics t
                                     :update-callback 'scene-update)))
    (capi:display scene-view)))

;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "USER")

(defpackage "HELLO-TRIANGLE-TEX"
  (:use "CAPI" "OPENGL" "SCENE" "CAPI-3D-KIT")
  (:add-use-defaults)
  (:export
   START-HELLO-TRIANGLE))

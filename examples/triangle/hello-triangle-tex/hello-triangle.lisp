;; Copyright (c) 2015 Deepak Surti. All rights reserved.

;******************************************************************************
; OpenGL 4 Example Code.
; Accompanies written series "Anton's OpenGL 4 Tutorials"
; Email: anton at antongerdelan dot net
; First version 27 Jan 2014
; Copyright Dr Anton Gerdelan, Trinity College Dublin, Ireland.
; See individual libraries for separate legal notices
;******************************************************************************
; Ported to LispWorks CAPI OpenGL interface
; Antons Source: 03_vertex_buffer_objects
; "Hello Triangle With Vertex Colors". Just the basics.
; Email: dmsurti at gmail dot com
;******************************************************************************
; THIS IS THE MOST BASIC DEMO, HELLO WORLD EQUIVALENT
;******************************************************************************
(in-package "HELLO-TRIANGLE-TEX")

(defun make-hello-triangle ()
  (let* ((v1 (make-vertex3  0.0  0.7 0.0))
         (v2 (make-vertex3  0.7 -0.7 0.0))
         (v3 (make-vertex3 -0.7 -0.7 0.0))
         (diffuse-texture-path (merge-pathnames "diffuse.png"
                                                *example-textures-dir*))
         (diffuse-texture (make-texture diffuse-texture-path))
         (mat (make-material (make-black-color)
                             (make-rgba 0.0 0.0 0.0)
                             diffuse-texture
                             (make-white-color)
                             100.0)))
    (setf (pos-diffuse-st v1) (make-tex-st 0.5 1.0 t)
          (pos-diffuse-st v2) (make-tex-st 1.0 0.0 t)
          (pos-diffuse-st v3) (make-tex-st 0.0 0.0 t))
    (let ((tri (make-triangle v1 v2 v3)))
      (setf (geometry-material tri) mat)
      tri)))

(defun make-triangle-scene ()
  (let* ((scene (make-scene))
         (triangle (make-hello-triangle))
         (tri-node (make-scene-node :geometry triangle)))
    (add-node scene tri-node)
    scene))

(defun start-hello-triangle ()
  (let ((scene-view (make-scene-view :scene-fn #'make-triangle-scene
                                     :show-console t
                                     :show-statistics t)))
    (capi:display scene-view)))

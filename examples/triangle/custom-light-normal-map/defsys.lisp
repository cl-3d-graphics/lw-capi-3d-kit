;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "CL-USER")

(defsystem "CUSTOM-LIGHT-NORMAL-MAP"
    ()
    :members
    (("SCENE" :type :system :root-module nil)
     ("CAPI-3D-KIT" :type :system :root-module nil)
     "hello-triangle"
     )
    :rules
    ((:in-order-to :compile :all (:requires (:load "SCENE")))
     (:in-order-to :compile :all (:requires (:load "CAPI-3D-KIT")))))

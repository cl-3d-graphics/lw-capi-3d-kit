;; Copyright (c) 2015 Deepak Surti. All rights reserved.

;******************************************************************************
; OpenGL 4 Example Code.
; Accompanies written series "Anton's OpenGL 4 Tutorials"
; Email: anton at antongerdelan dot net
; First version 27 Jan 2014
; Copyright Dr Anton Gerdelan, Trinity College Dublin, Ireland.
; See individual libraries for separate legal notices
;******************************************************************************
; Ported to LispWorks CAPI OpenGL interface
; Antons Source: 08_phong
; "Hello Triangle With Vertex Colors". Just the basics.
; Email: dmsurti at gmail dot com
;******************************************************************************
; THIS DEMOS THE USE OF AMBIENT LIGHT WITH MATERIAL
; IF THERE IS AMBIENT LIGHT PROVIDED, SCENE DEFAULT LIGHTING STATE IS REDUNDANT
;******************************************************************************

(in-package "DEFAULT-AMB-LIGHT-MAT")

(defun make-hello-triangle ()
  (let* ((v1 (make-vertex3  0.0  0.7 0.0 (make-vector3 0.0 0.0 1.0)))
         (v2 (make-vertex3  0.7 -0.7 0.0 (make-vector3 0.0 0.0 1.0)))
         (v3 (make-vertex3 -0.7 -0.7 0.0 (make-vector3 0.0 0.0 1.0)))
         (mat (make-material (make-black-color)
                             (make-white-color)
                             (make-rgba 1.0 0.5 0.0)
                             (make-white-color)
                             100.0))
         (tri (make-triangle v1 v2 v3)))
    (setf (geometry-material tri) mat)
    tri))

(defun make-triangle-scene ()
  (let* ((scene (make-scene))
         (amb-light (make-ambient-light (make-rgba 0.3 0.3 0.3)))
         (amb-light-node (make-node))
         (triangle (make-hello-triangle))
         (tri-node (make-scene-node :geometry triangle)))
    (add-node scene tri-node)
    (setf (node-light amb-light-node) amb-light)
    (add-node scene amb-light-node)
    scene))

(defun start-hello-triangle ()
  (let ((scene-view (make-scene-view :scene-fn #'make-triangle-scene
                                     :show-console t
                                     :show-statistics t)))
    (capi:display scene-view)))

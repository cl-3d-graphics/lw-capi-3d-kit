;; Copyright (c) 2015 Deepak Surti. All rights reserved.

;******************************************************************************
; OpenGL 4 Example Code.
; Accompanies written series "Anton's OpenGL 4 Tutorials"
; Email: anton at antongerdelan dot net
; First version 27 Jan 2014
; Copyright Dr Anton Gerdelan, Trinity College Dublin, Ireland.
; See individual libraries for separate legal notices
;******************************************************************************
; Ported to LispWorks CAPI OpenGL interface
; Antons Source: 08_phong
; "Hello Triangle With Vertex Colors". Just the basics.
; Email: dmsurti at gmail dot com
;******************************************************************************
; THIS DEMOS THE USE OF DEFAULT LIGHT WITH MATERIAL
;******************************************************************************

(in-package "DEFAULT-LIGHT-MAT-TEX")

(defun make-hello-triangle ()
  (let* ((v1 (make-vertex3  0.0  0.7 0.0 (make-vector3 0.0 0.0 1.0)))
         (v2 (make-vertex3  0.7 -0.7 0.0 (make-vector3 0.0 0.0 1.0)))
         (v3 (make-vertex3 -0.7 -0.7 0.0 (make-vector3 0.0 0.0 1.0)))
         (diffuse-texture-path (merge-pathnames "diffuse.png"
                                                *example-textures-dir*))
         (diffuse-texture (make-texture diffuse-texture-path))
         (specular-texture-path (merge-pathnames "specular.png"
                                                 *example-textures-dir*))
         (specular-texture (make-texture specular-texture-path))
         (mat (make-material (make-black-color)
                             (make-white-color)
                             diffuse-texture
                             specular-texture
                             100.0)))
    (setf (pos-diffuse-st v1) (make-tex-st 0.5 1.0 t)
          (pos-diffuse-st v2) (make-tex-st 1.0 0.0 t)
          (pos-diffuse-st v3) (make-tex-st 0.0 0.0 t)
          (pos-specular-st v1) (make-tex-st 0.5 1.0 t)
          (pos-specular-st v2) (make-tex-st 1.0 0.0 t)
          (pos-specular-st v3) (make-tex-st 0.0 0.0 t))
    (let ((tri (make-triangle v1 v2 v3)))
      ;; (setf (material-light-model mat) *phong*)
      (setf (geometry-material tri) mat)
      tri)))

(defun make-triangle-scene ()
  (let* ((scene (make-scene))
         (triangle (make-hello-triangle))
         (tri-node (make-scene-node :geometry triangle)))
    (add-node scene tri-node)
    (setf (scene-default-light scene) t)
    scene))

(defun start-hello-triangle ()
  (let ((scene-view (make-scene-view :scene-fn #'make-triangle-scene
                                     :show-console t
                                     :show-statistics t)))
    (capi:display scene-view)))

;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "CL-USER")

(load (current-pathname "../../../load"))

(load (current-pathname "pkg"))
(load (current-pathname "defsys"))

(compile-system "DEFAULT-LIGHT-COLOR-MAT" :load t)

#version 410

layout (location = 0)  in vec3 vertex_position;
layout (location = 2)  in vec3 vertex_normal;
layout (location = 9)  in vec3 vertex_tangent;
layout (location = 10) in vec3 vertex_bi_tangent;
layout (location = 3)  in vec4 vertex_color;
layout (location = 4)  in vec2 kd_vt;
layout (location = 5)  in vec2 ks_vt;
layout (location = 6)  in vec2 ka_vt;
layout (location = 7)  in vec2 ke_vt;
layout (location = 8)  in vec2 normal_vt;

uniform mat4 projection_mat, view_mat, model_mat;
out vec3 position_eye, normal_eye;
out mat3 tbn_mat;
out vec4 initial_color;
out vec2 ke_tex_coordinates;
out vec2 ka_tex_coordinates;
out vec2 kd_tex_coordinates;
out vec2 ks_tex_coordinates;
out vec2 normal_tex_coordinates;

void main () {
  ke_tex_coordinates = ke_vt;
  ka_tex_coordinates = ka_vt;
  kd_tex_coordinates = kd_vt;
  ks_tex_coordinates = ks_vt;
  normal_tex_coordinates = normal_vt;
	position_eye = vec3 (view_mat * model_mat * vec4 (vertex_position, 1.0));
	normal_eye = vec3 (view_mat * model_mat * vec4 (vertex_normal, 0.0));
  initial_color = vertex_color;
  tbn_mat = mat3(vertex_tangent.x, vertex_tangent.y, vertex_tangent.z,
                 vertex_bi_tangent.x, vertex_bi_tangent.y, vertex_bi_tangent.z,
                 vertex_normal.x, vertex_normal.y, vertex_normal.z);
	gl_Position = projection_mat * vec4 (position_eye, 1.0);
}
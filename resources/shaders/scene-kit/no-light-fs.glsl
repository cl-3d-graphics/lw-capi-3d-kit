#version 410

in vec3 color;
in vec2 texture_coordinates;

uniform sampler2D basic_texture;
uniform vec3 input_color;
uniform int use_color;
uniform int use_color_texture;

out vec4 frag_color;

void main() {
  if (use_color == 1)
  {
    frag_color = vec4(color, 1.0);
  } else
  {
    if (use_color_texture == 1)
    {
	    vec4 texel = texture (basic_texture, texture_coordinates);
	    frag_color = texel;
    }
    else
    {
      frag_color = vec4(input_color, 1.0);
    }
  }
}
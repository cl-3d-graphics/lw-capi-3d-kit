#version 410

layout(location = 0) in vec3 vertex_position;
layout(location = 3) in vec3 vertex_color;
layout(location = 4) in vec2 vt; // per-vertex texture co-ords

uniform mat4 proj, view, model;

out vec3 color;
out vec2 texture_coordinates;

void main() {
	color = vertex_color;
	texture_coordinates = vt;
	gl_Position = proj * view * model * vec4(vertex_position, 1.0);
}
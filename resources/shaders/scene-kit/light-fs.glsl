#version 410

#define MAX_LIGHTS 8

struct LightSource
{
  int Type;
  vec3 Position;
  float ConstAtt;
  float LinAtt;
  float QuadAtt;
  vec3 Direction;
  float ConeAngle;
  float SpotAtt;
  vec3 Color;
};

// allow only maximum of 8 lights
uniform LightSource Lights[MAX_LIGHTS];

in vec3 position_eye, normal_eye;
in mat3 tbn_mat;
in vec4 initial_color;
in vec2 ke_tex_coordinates;
in vec2 ka_tex_coordinates;
in vec2 kd_tex_coordinates;
in vec2 ks_tex_coordinates;
in vec2 normal_tex_coordinates;

uniform mat4 view_mat, model_mat;
uniform int use_color;
uniform int light_model;

// ambient light intensity
uniform vec3 ambient_light;

// surface reflectance
uniform int use_ke_texture;
uniform sampler2D ke_texture;
uniform vec3 Ke;

uniform int use_ks_texture;
uniform sampler2D ks_texture;
uniform vec3 Ks;

uniform int use_kd_texture;
uniform sampler2D kd_texture;
uniform vec3 Kd;

uniform int use_ka_texture;
uniform sampler2D ka_texture;
uniform vec3 Ka;

uniform int use_normal_texture;
uniform sampler2D normal_texture;
uniform vec3 Ncolor;

uniform float specular_exponent;

out vec4 fragment_colour; // final colour of surface

void main () {
  vec4 texel;
  vec3 n_eye;
  vec3 light_position_world;
  vec3 light_position_eye, distance_to_light_eye, direction_to_light_eye;
  vec3 light_direction_world;
  vec3 light_direction_eye;
  float angle, cone_angle, cutoff, spot_factor;
  float d, k_c, k_d, k_q, dist_attenuation, spot_attenuation;
  vec3 Ld, Ls;

	// ambient intensity
  vec3 Ia;
  if (use_ka_texture == 0)
  {
	  Ia = ambient_light * Ka;
  } else
  {
	  texel = texture (ka_texture, ka_tex_coordinates);
    Ia = ambient_light * vec3(texel);
  }

  // diffuse and specular intensity across lights
  vec3 Id = vec3(0.0, 0.0, 0.0);
  vec3 Is = vec3(0.0, 0.0, 0.0);

  for (int i = 0; i < 8; i++)
  {
    if (Lights[i].Type == 1)
    {
      light_position_world = Lights[i].Position;
      light_position_eye = vec3 (view_mat * vec4 (light_position_world, 1.0));
      distance_to_light_eye = light_position_eye - position_eye;
      direction_to_light_eye = normalize (distance_to_light_eye);
      //attentuation
      d = distance(light_position_eye, position_eye);
      k_c = Lights[i].ConstAtt;
      k_d = Lights[i].LinAtt;
      k_q = Lights[i].QuadAtt;
      dist_attenuation += 1 / (k_c + k_d * d + k_q * d * d);
    }

    if (Lights[i].Type == 2)
    {
      light_direction_world = Lights[i].Direction;
      light_direction_eye = vec3(view_mat * vec4 (light_direction_world, 1.0));
      direction_to_light_eye = normalize(light_direction_eye);
      dist_attenuation = 1.0; // no attenuation
    }

    spot_factor = 1.0; // initialize spot factor to 1
    if (Lights[i].Type == 3)
    {
      light_position_world = Lights[i].Position;
      light_position_eye = vec3 (view_mat * vec4 (light_position_world, 1.0));
      distance_to_light_eye = light_position_eye - position_eye;
      direction_to_light_eye = normalize (distance_to_light_eye);
      //attentuation
      k_c = Lights[i].ConstAtt;
      k_d = Lights[i].LinAtt;
      k_q = Lights[i].QuadAtt;
      dist_attenuation += 1 / (k_c + k_d * d + k_q * d * d);

      light_direction_world = Lights[i].Direction;
      light_direction_eye = vec3(view_mat * vec4 (light_direction_world, 1.0));
      light_direction_eye = normalize(light_direction_eye);
      angle = acos(dot(-direction_to_light_eye, light_direction_eye));
      cone_angle = degrees(Lights[i].ConeAngle);
      cutoff = radians(clamp(cone_angle, 0, 90.0));
      spot_attenuation = Lights[i].SpotAtt;
      if (angle < cutoff) {
        spot_factor = pow(dot (-direction_to_light_eye, light_direction_eye),
                          spot_attenuation);
      } else {
        spot_factor = 0.0;
      }
    }

    // If normal mapping is used, compute normal_eye from tangent space
    if (use_normal_texture == 1)
    {
      vec3 normal_tan = texture(normal_texture, normal_tex_coordinates).rgb;
      normal_tan = normalize(2.0 * normal_tan - 1.0);
      vec4 normal_local = vec4(tbn_mat * normal_tan, 0.0);
	    n_eye = vec3 (view_mat * model_mat * normal_local);
      n_eye = -n_eye;
    } else
    {
      n_eye = normal_eye;
    }

    // diffuse intensity
    // raise light position to eye space
    float dot_prod = dot (direction_to_light_eye, n_eye);
    dot_prod = max (dot_prod, 0.0);
    Ld = Lights[i].Color;
	  Id += Ld * dot_prod  * spot_factor * dist_attenuation;

    // specular intensity
    vec3 surface_to_viewer_eye = normalize (-position_eye);

  	// blinn-phong
    float dot_prod_specular, specular_factor;
    if (light_model == 1)
    {
  	  vec3 half_way_eye = normalize (surface_to_viewer_eye + direction_to_light_eye);
      dot_prod_specular = max (dot (half_way_eye, n_eye), 0.0);
  	  specular_factor = pow (dot_prod_specular, specular_exponent);
    }
  	
    // phong
    if (light_model == 2)
    {
      vec3 reflection_eye = reflect (-direction_to_light_eye, n_eye);
      dot_prod_specular = dot (reflection_eye, surface_to_viewer_eye);
      dot_prod_specular = max (dot_prod_specular, 0.0);
      specular_factor = pow (dot_prod_specular, specular_exponent);
    }
  
    Ls = Lights[i].Color;
	  Is += Ls * specular_factor * spot_factor * dist_attenuation;
  }

	// final diffuse intensity
  if (use_kd_texture == 0)
  {
	   Id *= Kd;
  } else
  {
	   texel = texture (kd_texture, kd_tex_coordinates);
	   Id *= vec3(texel);
  }

  // final specular intensity
  if (use_ks_texture == 0)
  {
	  Is *= Ks;
  } else
  {
	  texel = texture (ks_texture, ks_tex_coordinates);
	  Is *= vec3(texel);
  }
	
	// final colour
  if (use_ke_texture == 0)
  {
    if (use_color == 1)
    {
      fragment_colour = vec4(Ke, 1.0) + vec4((Ia + Id), 1.0) * initial_color + vec4(Is, 1.0);
    } else
    {
	    fragment_colour = vec4 (Ke + Is + Id + Ia, 1.0);
    }
  } else
  {
	  texel = texture (ke_texture, ke_tex_coordinates);
    if (use_color == 1)
    {
      fragment_colour = vec4(vec3(texel), 1.0) + vec4((Ia + Id), 1.0) * initial_color + vec4(Is, 1.0);
    } else
    {
	    fragment_colour = vec4 (vec3(texel) + Is + Id + Ia, 1.0);
    }
  }
}
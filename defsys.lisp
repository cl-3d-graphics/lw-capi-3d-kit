;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "USER")

(defsystem "CAPI-3D-KIT"
  (:optimize ((debug 3) (safety 3)))
  :members (("OPENGL" :type :system :root-module nil)
            ("SCENE" :type :system :root-module nil)
            "pkg"
            "resources"
            "src/utils/sk-matrix"
            "src/utils/sk-quat"
            "src/utils/sk-types"
            "src/geometry/sk-texture"
            "src/geometry/sk-material"
            "src/geometry/sk-source"
            "src/geometry/sk-geometry"
            "src/geometry/sk-shader-lights"
            "src/geometry/sk-shader-locs"
            "src/geometry/sk-shaders"
            "src/geometry/sk-node"
            "src/scene/sk-camera"
            "src/scene/sk-scene"
            "src/view/sk-interface")
  :rules ((:in-order-to :load :all
                        (:requires (:load :serial)))
          (:in-order-to :compile :all
                        (:caused-by (:compile :previous))
                        (:requires (:load :serial))))
  )




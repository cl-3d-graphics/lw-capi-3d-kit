;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "CAPI-3D-KIT")

(defparameter *project-dir* (current-pathname))

(defparameter *button-img-dir*
  (merge-pathnames #p"resources/images/buttons/"
                   *project-dir*))

;;; Register all images
(register-image 'color-picker "paint4.png" *button-img-dir*)

(defparameter *shaders-dir*
  (merge-pathnames #p"resources/shaders/"
                   *project-dir*))

(defparameter *scene-kit-shaders-dir*
  (merge-pathnames #p"scene-kit/"
                   *shaders-dir*))

(defparameter *example-textures-dir*
  (merge-pathnames #p"resources/images/textures/"
                   *project-dir*))

;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "CL-USER")

(load (current-pathname "../cl-dae-parser/load"))
(load (current-pathname "../cl-scene/load"))
(load (current-pathname "../capi-opengl/load"))

(load (current-pathname "defsys"))

(compile-system "CAPI-3D-KIT" :load t)
